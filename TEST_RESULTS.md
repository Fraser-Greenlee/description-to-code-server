
# Results of different model types on the module, compiled CoNaLa dataset.

## GRU Default

All given 75,000 iterations on a dataset of 2523 unique description-code pairs (so ~30 epochs).
Defaults are: GRU network, 256 hidden nodes, 1 layer, 0.5 teacher forcing.

| Difference | Accuracy (%) | Difference |
|---|---|---|
| LSTM network | 76.9 | +30.1 |
| 2 layers | 54.6 | +7.8 |
| 266 hidden nodes | 49.4 | +2.9 |
| **Default** | 46.8 |  |
| 246 hidden nodes | 45.9 | -0.9 |
| 0.3 teacher forcing | 33.9 | -12.9 |

## LSTM Default

All given exactly 30 epochs on 2523 pairs.
Defaults are: LSTM network, 256 hidden nodes, 1 layer, 0.5 teacher forcing.

*Found that accuracy results were only accurate to an integer precentage varying +-0.4%*

| Difference | Accuracy (%) |
|---|---|
| 39 epochs                 | 97 |
| 36 epochs                 | 95 |
| 33 epochs 266 hidden      | 91 |
| 33 epochs                 | 89 |
| 266 hidden nodes          | 84 |
| 276 hidden nodes          | 83 |
| 2 layers                  | 78 |
| **Default**               | 76 |
| 27 epochs                 | 70 |
| 3 layers                  | 43 |


| Difference | Raw measured Accuracy (%) |
|---|---|
| 39 epochs | 97.9, 97.2 |
| 36 epochs | 95 |
| 33 epochs 266 hidden | 91.7 |
| 33 epochs | 89.2 |
| 266 hidden nodes | 84.7 |
| 276 hidden nodes | 83.2 |
| 2 layers | 78.4 |
| **Default** | 76.9 |
| 27 epochs | 70.7 |
| 3 layers | 43.1 |

## LSTM Expanded Dataset

| Difference | Accuracy (%) |
|---|---|
| 2 epochs | 96.2 |
| 1 epoch | 86.4 |

import unittest

from old_parameters import find_parameters, new_parameter_key, _find_code_parameters, replace_parameters
from constants import (
    PARAMETER_STRING,
    PARAMETER_NUMBER,
    PARAMETER_VARIABLE
)
NUMBER_0 = new_parameter_key(PARAMETER_NUMBER, 0)
NUMBER_1 = new_parameter_key(PARAMETER_NUMBER, 1)
NUMBER_2 = new_parameter_key(PARAMETER_NUMBER, 2)

STRING_0 = new_parameter_key(PARAMETER_STRING, 0)
STRING_1 = new_parameter_key(PARAMETER_STRING, 1)
STRING_2 = new_parameter_key(PARAMETER_STRING, 2)

VARIABLE_0 = new_parameter_key(PARAMETER_VARIABLE, 0)
VARIABLE_1 = new_parameter_key(PARAMETER_VARIABLE, 1)
VARIABLE_2 = new_parameter_key(PARAMETER_VARIABLE, 2)


class TestParametersMethods(unittest.TestCase):

    def test_find_parameters_number(self):
        parameter_map, parameterized_description = find_parameters("create 3 random numbers from -2 to 10.5".split(), [])
        self.assertEqual(
            parameter_map,
            {
                NUMBER_0: '3',
                NUMBER_1: '-2',
                NUMBER_2: '10.5'
            }
        )
        self.assertEqual(
            parameterized_description,
            "create NUMBER_0 random numbers from NUMBER_1 to NUMBER_2".split()
        )

    def test_find_parameters_string(self):
        parameter_map, parameterized_description = find_parameters("print 'a' and \"b\" and '/|\\!@£$%^&*()'".split(), [])
        self.assertEqual(
            parameter_map,
            {
                STRING_0: '"a"',
                STRING_1: '"b"',
                STRING_2: '"/|\\!@£$%^&*()"'
            }
        )
        self.assertEqual(
            parameterized_description,
            "print STRING_0 and STRING_1 and STRING_2".split()
        )

    def test_insert_parameters_number(self):
        words = replace_parameters(
            {
                NUMBER_0: '3',
                NUMBER_1: '2',
                NUMBER_2: '10.5'
            },
            ["create", NUMBER_0, "random", "numbers", "from", NUMBER_1, "to", NUMBER_2]
        )
        self.assertEqual(
            words,
            ["create", "3", "random", "numbers", "from", "2", "to", "10.5"]
        )

    def test_insert_parameters_string(self):
        words = replace_parameters(
            {
                STRING_0: "'a big'",
                STRING_1: "'numbers'",
                STRING_2: "'2 to 10.5'"
            },
            ["create", STRING_0, "random", STRING_1, "from", STRING_2]
        )
        self.assertEqual(
            words,
            ["create", "'a big'", "random", "'numbers'", "from", "'2 to 10.5'"]
        )

    def test_find_code_parameters_vars_and_numbers(self):
        code_tokens = _find_code_parameters(
            {
                VARIABLE_0: "i",
                NUMBER_0: "3"
            },
            ["i"],
            'i = i * 3 * 2'.split()
        )
        self.assertEqual(
            code_tokens,
            [VARIABLE_0, '=', VARIABLE_0, '*', NUMBER_0, '*', '2']
        )

    def test_find_code_parameters_strings(self):
        code_tokens = _find_code_parameters(
            {
                VARIABLE_0: "obj",
                STRING_0: '"path"'
            },
            ["obj"],
            "with open ( 'path' , 'w' ) as outfile : \n json . dump ( obj , outfile )".split()
        )
        self.assertEqual(
            code_tokens,
            'with open ( STRING_0 , "w" ) as outfile : \n json . dump ( VARIABLE_0 , outfile )'.split()
        )

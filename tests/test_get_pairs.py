import unittest

from get_pairs import normalize_input_description


class TestPGetPairsMethods(unittest.TestCase):

    def test_normalize_input_description_numbers(self):
        normalized_description = normalize_input_description(
            'rAndom numBer from -7 to 12.5!!!'
        )
        self.assertEqual(
            normalized_description,
            'random number from -7 to 12.5'
        )

    def test_normalize_input_description_strings(self):
        normalized_description = normalize_input_description(
            'PrINT my $string \'!@£$%^&*()\' and print "hello"'
        )
        self.assertEqual(
            normalized_description,
            'print my string \'!@£$%^&*()\' and print "hello"'
        )

    def test_normalize_input_description_date(self):
        normalized_description = normalize_input_description(
            "Getting today's date in YYYY-MM-DD in Python?"
        )
        self.assertEqual(
            normalized_description,
            "getting today s date in yyyy -mm -dd in python"
        )

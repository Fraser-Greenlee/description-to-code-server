from random import randint
import json
from nltk.corpus import stopwords as stpwrds

from _constants import (
    MAX_PERMUTATIONS_PER_SAMPLE,
    MAX_PERMUTATIONS_RESAMPLING,
    DESCRIPTION_INDEX,
    CODE_INDEX,
    SYNONYMS_PATH
)
SYNONYMS = json.load(open(SYNONYMS_PATH))
STOPWORDS = set(stpwrds.words('english'))


def _remove_double_spaces(a_string):
    return a_string.replace('   ', ' ').replace('  ', ' ').strip()


def _find_synonyms(description):
    tokens = description.split(' ')
    synonym_tokens = []
    for token in tokens:
        if token in SYNONYMS:
            synonym_tokens.append(SYNONYMS[token])
        elif token in STOPWORDS:
            synonym_tokens.append([token, ''])
        else:
            synonym_tokens.append([token])
    return synonym_tokens


def _next_index(index, full_index):
    for i, _ in enumerate(index):
        if index[i] < full_index[i]:
            index[i] += 1
            index[:i] = [0] * i
            break
    return index


def _description_at_index(index, synonym_tokens):
    variation = []
    for i, synonyms in enumerate(synonym_tokens):
        token = synonyms[index[i]]
        if token and token[0].islower():
            token = ' '.join(token.split('_'))
        variation.append(token)
    description = _remove_double_spaces(' '.join(variation))
    return description


def _variations_on_pair(description, code):
    synonyms_for_each_token = _find_synonyms(description)

    variations = []
    variation_index = 1
    index = [0 for array in synonyms_for_each_token]
    full_index = [len(array) - 1 for array in synonyms_for_each_token]
    if full_index == index:
        return [
            [description, code]
        ]
    index[0] = -1
    while index != full_index:
        index = _next_index(index, full_index)
        description = _description_at_index(index, synonyms_for_each_token)

        if len(variations) < MAX_PERMUTATIONS_PER_SAMPLE:
            variations.append([description, code])
        else:
            variations[randint(0, MAX_PERMUTATIONS_PER_SAMPLE - 1)][DESCRIPTION_INDEX] = description
            if variation_index >= MAX_PERMUTATIONS_RESAMPLING:
                break
        variation_index += 1

    return variations


def make_variations(pairs):
    '''Make variations on the descriptions in a list of description-code pairs'''
    new_pairs = []
    for pair in pairs:
        new_pairs += _variations_on_pair(pair[DESCRIPTION_INDEX], pair[CODE_INDEX])
    return new_pairs

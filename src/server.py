import datetime
import os
import json
import torch
from flask import Flask, request, jsonify
from apscheduler.schedulers.background import BackgroundScheduler
from bson import ObjectId

from _constants import (
    PORT,
    LANGUAGE_INSTANCES_PATH,
    DESCRIPTION_KEY,
    CODE_KEY,
    MODEL
)
from _scrape_source_code import variables_from_source_code
from _language import Description
from _volcab_error import VolcabError
from _load_pickle import load_pickle
from _db import PREDICTIONS
from _find_pair_tokens import parse_input_description, parse_a_text_pair
from _new_token import insert_new_tokens
from _evaluate import Evaluator
from _tokenize_python import join_code_tokens
SERVER = Flask(__name__)
device = torch.device("cuda" if torch.cuda.is_available() else "cpu")
DESCRIPTION_LANG, CODE_LANG = load_pickle(LANGUAGE_INSTANCES_PATH)
encoder, decoder = torch.load(MODEL)
EVALUATOR = Evaluator(device, DESCRIPTION_LANG, CODE_LANG, encoder, decoder)



def _parse_get_code(request_json):
    raw_description = request_json['description']
    if not raw_description:
        raise RuntimeError('empty description')

    variables = variables_from_source_code(request_json['source_code'], request_json['line_number'])
    parameter_replacement_map, parameterized_description = parse_input_description(raw_description, variables)

    is_unsure, parameterized_code_tokens = EVALUATOR.evaluate_handling_tensors(parameterized_description)
    print('code tokens:', ' '.join(parameterized_code_tokens))
    if is_unsure:
        return is_unsure, None, raw_description, None

    raw_code = EVALUATOR.replace_code_using_map(parameterized_code_tokens, parameter_replacement_map)
    parameterized_code = join_code_tokens(parameterized_code_tokens[:-1])

    return is_unsure, parameterized_description, parameterized_code, raw_code


def _save_initial_prediction(raw_description):
    row = {
        'description': {
            'parameterized': None,
            'raw': raw_description
        },
        'code': {
            'generated': None,
            'confirmed': {
                'parameterized': None,
                'raw': None
            }
        },
        'context': {
            'modules': [],
            'variables': []
        },
        'got_response': False,
        'attempted_to_train_on': False,
        'added_at': datetime.datetime.now()
    }
    inserted_response = PREDICTIONS.insert_one(row)
    return str(inserted_response.inserted_id)



def _update_initial_prediction(prediction_id, parameterized_description, parameterized_code, raw_code):
    code_dict = {
        'code': {
            'generated': {
                'parameterized': parameterized_code,
                'raw': raw_code
            },
        }
    }
    PREDICTIONS.update_one({'_id': ObjectId(prediction_id)}, {'$set': {'description.parameterized': parameterized_description}})
    PREDICTIONS.update_one({'_id': ObjectId(prediction_id)}, {'$set': code_dict})


@SERVER.route('/get_code', methods=['POST'])
def get_code():
    prediction_id = _save_initial_prediction(request.json['description'])
    try:
        is_unsure, parameterized_description, parameterized_code, raw_code = _parse_get_code(request.json)
    except RuntimeError as error:
        return jsonify({
            'error': str(error),
            'message': 'Bad input, try another description.'
        })
    except VolcabError as volcab_error:
        if isinstance(volcab_error.language, Description):
            # insert_new_tokens(DESCRIPTION_KEY, volcab_error.breaking_tokens)
            return jsonify({
                'error': 'Unknown tokens',
                'message': 'desc2code is not familiar with the words "{0}".'.format(', '.join(volcab_error.breaking_tokens))
            })
        return jsonify({
            'error': 'Bad error',
            'message': 'Bad error, try again.'
        })

    if not is_unsure and parameterized_code is not False:
        _update_initial_prediction(
            prediction_id, parameterized_description, parameterized_code, raw_code
        )
    else:
        return jsonify({
            'error': 'Unsure error',
            'message': 'Model unsure, try another description.'
        })
    return jsonify({
        'code': raw_code,
        'prediction_id': prediction_id
    })


def _prediction_response_params(request_json):
    prediction_id = request_json['prediction_id']
    prediction = PREDICTIONS.find_one({'_id': ObjectId(prediction_id)})
    raw_code = request_json['code']
    return prediction_id, prediction, raw_code


def _update_prediction(prediction_id, prediction):
    PREDICTIONS.update_one(
        {'_id': ObjectId(prediction_id)},
        {
            '$set': {
                'code': prediction['code'],
                'got_response': True
            }
        }
    )


@SERVER.route('/save_prediction_response', methods=['POST'])
def save_prediction_response():
    prediction_id, prediction, raw_code = _prediction_response_params(request.json)
    prediction['code']['confirmed']['raw'] = raw_code
    _update_prediction(prediction_id, prediction)
    return jsonify({
        'completed': True
    })


@SERVER.route('/save_object', methods=['POST'])
def save_object():
    ids = []
    for file_name in os.listdir('user_study_keylogs/'):
        if file_name[0] != '.':
            ids.append(int(file_name[:-5]))
    max_id = sorted(ids)[-1]
    new_id = max_id + 1
    data = request.json
    with open('user_study_keylogs/' + str(new_id) + '.json', 'w') as outfile:
        json.dump(data, outfile)
    return ''


if __name__ == "__main__":
    SERVER.run(port=PORT)


class VolcabError(Exception):
    '''
    Special exception for a volcabulary miss.

    attributes:
        `language`: Description/Code instance showing the language of the tokens.
        `breaking_token`: lets you know what the breaking words were.
    '''
    def __init__(self, message, language, tokens):
        super().__init__(message)
        self.language = language
        self.breaking_tokens = tokens

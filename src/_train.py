import time
import random
import torch
from torch import nn
from torch import optim

from _constants import (
    TEACHER_FORCING_RATIO,
    MAX_STRING_LENGTH,
    SOS_TOKEN,
    EOS_TOKEN,
    MODEL_EPOCH_TEMPLATE,
    DATASET_SPLIT_MAX_VALUE
)
from _evaluate import Evaluator
from _make_tensor import tensors_from_indeces_file
from _time_keeping import time_since


class Train:
    def __init__(self, device, encoder, decoder, learning_rate=0.01):
        self.device = device
        self.encoder = encoder
        self.decoder = decoder
        self.learning_rate = learning_rate
        self.encoder_optimizer = optim.SGD(self.encoder.parameters(), lr=self.learning_rate)
        self.decoder_optimizer = optim.SGD(self.decoder.parameters(), lr=self.learning_rate)
        self.criterion = nn.NLLLoss()


    def _train(self, input_tensor, target_tensor, max_length=MAX_STRING_LENGTH):
        encoder_hidden = self.encoder.init_hidden()

        self.encoder_optimizer.zero_grad()
        self.decoder_optimizer.zero_grad()

        input_length = input_tensor.size(0)
        target_length = target_tensor.size(0)

        encoder_outputs = torch.zeros(max_length, self.encoder.hidden_size, device=self.device)

        loss = 0

        for e_i in range(input_length):
            encoder_output, encoder_hidden = self.encoder(input_tensor[e_i], encoder_hidden)
            encoder_outputs[e_i] = encoder_output[0, 0]

        decoder_input = torch.tensor([[SOS_TOKEN]], device=self.device)

        decoder_hidden = encoder_hidden

        use_teacher_forcing = True if random.random() < TEACHER_FORCING_RATIO else False

        if use_teacher_forcing:
            # Teacher forcing: Feed the target as the next input
            for decoder_i in range(target_length):
                decoder_output, decoder_hidden, _ = self.decoder(decoder_input, decoder_hidden, encoder_outputs)
                loss += self.criterion(decoder_output, target_tensor[decoder_i])
                decoder_input = target_tensor[decoder_i]  # Teacher forcing

        else:
            # Without teacher forcing: use its own predictions as the next input
            for decoder_i in range(target_length):
                decoder_output, decoder_hidden, _ = self.decoder(
                    decoder_input, decoder_hidden, encoder_outputs
                )
                _, topi = decoder_output.topk(1)
                decoder_input = topi.squeeze().detach()  # detach from history as input

                loss += self.criterion(decoder_output, target_tensor[decoder_i])
                if decoder_input.item() == EOS_TOKEN:
                    break

        loss.backward()

        self.encoder_optimizer.step()
        self.decoder_optimizer.step()

        return loss.item() / target_length


    def _train_on_tensor_pairs(self, training_pairs, print_loss_total):
        for _, training_pair in enumerate(training_pairs):
            loss = self._train(training_pair[0], training_pair[1])
            print_loss_total += loss
        return print_loss_total


    def train_epochs(self, n_epochs, print_every=5):
        '''Train the model on pairs from pairs_collection for n_epochs.'''
        print('Training Started')
        print_loss_total = 0  # Reset every `self.print_every`
        start = time.time()
        for epoch_i in range(n_epochs):
            for split_index in range(DATASET_SPLIT_MAX_VALUE):
                training_pairs = tensors_from_indeces_file(self.device, split_index)
                print_loss_total = self._train_on_tensor_pairs(training_pairs, print_loss_total)

                if (split_index + 1) % print_every == 0:
                    split_point = split_index + 1
                    print_loss_avg = print_loss_total / print_every
                    print_loss_total = 0
                    print(
                        '%s (%d %d%%) %.4f' % (time_since(start, split_point / DATASET_SPLIT_MAX_VALUE), split_point, split_point / DATASET_SPLIT_MAX_VALUE * 100, print_loss_avg)
                    )

            torch.save(
                [self.encoder, self.decoder],
                MODEL_EPOCH_TEMPLATE.format(str(epoch_i))
            )


    def tune_model(self, corrected_tensor_pairs):
        evaluator = Evaluator(self.device, None, None, self.encoder, self.decoder)
        track_accuracy = []
        for _ in range(20):
            self._train_on_tensor_pairs(corrected_tensor_pairs, 0)
            passed = True
            for _ in range(4):
                accuracy = evaluator.accuracy_on_pairs(corrected_tensor_pairs)
                track_accuracy.append(accuracy)
                if accuracy < 94:
                    passed = False
                    break
            if passed:
                break
        return passed

import ast
import re
import random

from _tokenize_python import py_tokenize
from _constants import (
    DESCRIPTION_INDEX,
    CODE_INDEX,
    P_STRING,
    P_NUMBER,
    P_LIST,
    P_DICT,
    P_SET,
    P_TUPLE,
    P_VARIABLE,
    P_KEY,
    PARAMETER_PREFIXES,
    NUMBER_OF_EACH_PARAMETER_TYPE,
    NUMBERS
)
from _built_in_methods import BUILTIN_METHODS
BRACKET_STARTERS = [
    '[',
    '{',
    '('
]
BRACKET_ENDERS = [
    ']',
    '}',
    ')'
]
QUOTES = [
    "'",
    '"',
    '`'
]
LEAF_TYPES = (
    ast.UnaryOp,
    ast.List,
    ast.Dict,
    ast.Tuple,
    ast.Set,
    ast.Str,
    ast.Num,
    ast.Load
)
AST_TO_PARAMETER_TYPE = {
    ast.UnaryOp: P_NUMBER,
    ast.Num: P_NUMBER,
    ast.Name: P_VARIABLE,
    ast.List: P_LIST,
    ast.Dict: P_DICT,
    ast.Tuple: P_TUPLE,
    ast.Set: P_SET,
    ast.Str: P_STRING
}
PLAIN_TEXT = 'plain text'
DESCRIPTION = 'description'
CODE = 'code'
BUILTIN_FUNCTIONS = []


class Segmentor:
    '''
    Segments a given description into a list of strigs within/(not within) quotes and brackets.
    e.g.
        >"say 'hello' (to) eveyryone"
        segments =  ['say ', "'hello'", ' ', '(to)', ' everyone']
    '''
    def __init__(self, description):
        if not description:
            raise Exception('Empty description.')
        self.description = description
        self.segments = []
        self.segmentor_stack = []
        self.last_segment_i = 0

    def _matching_segment(self, char):
        if not self.segmentor_stack:
            return False
        stack_char = self.segmentor_stack[-1]
        if stack_char in QUOTES:
            return stack_char == char
        return BRACKET_ENDERS[BRACKET_STARTERS.index(stack_char)] == char

    def _append_segment(self, within_quote_bracket, i):
        up_to = i+1 if within_quote_bracket else i
        self.segments.append(
            self.description[self.last_segment_i: up_to]
        )
        self.last_segment_i = i+1 if within_quote_bracket else i

    def find_segments(self):
        '''
        Find the segments lists for the given description.
        '''
        for i, char in enumerate(self.description):
            if char in QUOTES and (self.description[i - 1] != '\\' or self.description[i - 2] == '\\'):
                if self._matching_segment(char):
                    self._append_segment(True, i)
                    self.segmentor_stack.pop()
                elif not self.segmentor_stack and self.description[i - 1] == ' ':
                    self._append_segment(False, i)
                    self.segmentor_stack.append(char)

            elif not self.segmentor_stack or self.segmentor_stack[-1] not in QUOTES:
                if char in BRACKET_STARTERS:
                    if not self.segmentor_stack:
                        self._append_segment(False, i)
                    self.segmentor_stack.append(char)
                elif char in BRACKET_ENDERS:
                    if self._matching_segment(char):
                        self.segmentor_stack.pop()
                        if not self.segmentor_stack:
                            self._append_segment(True, i)
                    else:
                        print(self.description)
                        raise Exception('Unexpected closing bracket.')
        if not self.segmentor_stack:
            self.segments.append(
                self.description[self.last_segment_i:]
            )
        else:
            raise Exception('Unfinished segmentor stack.')
        return self.segments


def _normalize_string(string):
    string = string.lower().strip()
    string = re.sub(r"([!?()])", r" ", string)
    string = re.sub(r"[^a-zA-Z0-9.-]+", r"", string)
    return string.strip()


def _in_quotes(text):
    return text[0] == text[-1] and text[0] in QUOTES


def _in_brackets(text):
    return text[0] in BRACKET_STARTERS and text[-1] == BRACKET_ENDERS[BRACKET_STARTERS.index(text[0])]


class DescriptionCodePairParameterizor:
    '''
    Parameterizes description_segments and code, replacing tokens with STRING_0, LIST_1, etc
    '''
    def __init__(self, description_segments, code, variables, strings):
        self.segments = description_segments
        self.parsed_segments = []
        self.parameter_map = {}
        for prefix in PARAMETER_PREFIXES:
            self.parameter_map[prefix] = []
        self.code = code
        self.variables = variables
        self.strings = strings
        self.new_code = ' '.join(py_tokenize(code))

    def _classify_segment(self, text):
        is_quoted = _in_quotes(text)
        if is_quoted:
            unwraped = text[1:-1]
        if is_quoted and unwraped in self.strings:
            return P_STRING, "'{0}'".format(unwraped)
        if text in self.variables:
            return P_VARIABLE, text
        if is_quoted and unwraped in self.variables:
            return P_VARIABLE, unwraped
        p_type = _valid_python(text)
        if p_type:
            return p_type, text
        if is_quoted:
            p_type = _valid_python(unwraped)
            if p_type:
                return p_type, unwraped
        if is_quoted or _in_brackets(text):
            # Since text is not a quoted variable/list/etc it must be a string that is not in the code
            return PLAIN_TEXT, text[1:-1]
        # print('Cannont classify the segment: "{0}"'.format(str(text)))
        return False, False

    def _add_parameter(self, parameter_type, value):
        if value not in self.parameter_map[parameter_type]:
            self.parameter_map[parameter_type].append(value)
        p_type_count = len(self.parameter_map[parameter_type])
        if p_type_count > NUMBER_OF_EACH_PARAMETER_TYPE:
            raise ValueError('Too many parameters of type {0}.'.format(parameter_type))
        return new_parameter_key(parameter_type, p_type_count - 1)

    def _replace_code_token(self, current_token, replace_with):
        old_tokens = py_tokenize(self.new_code)
        new_tokens = []
        did_replace = False
        for token in old_tokens:
            if token == current_token or (token[0] in QUOTES and current_token[0] in QUOTES and token[1:-1] == current_token[1:-1]):
                new_tokens.append(replace_with)
                did_replace = True
            else:
                new_tokens.append(token)
        if not did_replace:
            # print('No token replaced.')
            return False
        self.new_code = ' '.join(new_tokens)
        return True

    def _replace_code_overlap_case(self):
        '''Handle case where one parameter overlaps over another'''
        code = ' '.join(py_tokenize(self.code))
        replace_pairs = []
        for key in self.parameter_map:
            for i, token in enumerate(self.parameter_map[key]):
                token = ' '.join(py_tokenize(token))
                p_key = new_parameter_key(key, i)
                replace_pairs.append((p_key, token))
                code = code.replace(p_key, token)
        i = 0
        worked = False
        while worked is False:
            worked = True
            changing_code = code
            random.shuffle(replace_pairs)
            for key, token in replace_pairs:
                if token not in changing_code:
                    worked = False
                    break
                else:
                    changing_code = changing_code.replace(token, key)
            i += 1
            if i > 20:
                raise Exception('Overlap case took too long.')
        return changing_code

    def _replace_code_substring(self, substring, replace_with):
        substring = ' '.join(py_tokenize(substring))
        old_code = self.new_code
        self.new_code = self.new_code.replace(substring, replace_with)
        if old_code == self.new_code:
            if substring in ' '.join(py_tokenize(self.code)) and substring not in self.new_code:
                self.new_code = self._replace_code_overlap_case()
            elif substring not in ' '.join(py_tokenize(self.code)):
                print('Substring not present in original code.')
                return False
        if old_code == self.new_code:
            raise Exception('Substring not replaced.')
        return True

    def _parameterize_quote_bracket_segment(self, segment):
        segment_class, segment = self._classify_segment(segment)
        if segment_class == PLAIN_TEXT:
            return self._parameterize_plain_text_segment(segment)
        if segment_class is False:
            raise Exception('Cannont classify the segment: "{0}"'.format(str(segment)))
        param_key = self._add_parameter(segment_class, segment)
        if segment_class in [P_LIST, P_DICT, P_KEY, P_SET, P_TUPLE, P_NUMBER]:
            if not self._replace_code_substring(segment, param_key):
                return self._parameterize_plain_text_segment(segment)
            return param_key
        else:
            worked = self._replace_code_token(segment, param_key)
            if worked:
                return param_key
        return self._parameterize_plain_text_segment(segment)

    def _parameterize_plain_text_segment(self, segment):
        new_tokens = []
        tokens = segment.split(' ')
        for token in tokens:
            if not token:
                continue
            if token[-1] == ',':
                token = token[:-1]
                if not token:
                    continue
            segment_class, possible_token = self._classify_segment(token)
            if segment_class == PLAIN_TEXT:
                continue
            if segment_class is False:
                token = _normalize_string(token)
            else:
                parameter_key = self._add_parameter(segment_class, possible_token)
                worked = self._replace_code_token(possible_token, parameter_key)
                if worked:
                    token = parameter_key
            new_tokens.append(token)
        return ' '.join(new_tokens)

    def _parameterize_segment(self, segment):
        if _in_quotes(segment) or _in_brackets(segment):
            return self._parameterize_quote_bracket_segment(segment)
        return self._parameterize_plain_text_segment(segment)

    def parameterize(self):
        new_segments = []
        for segment in self.segments:
            segment = segment.strip()
            if not segment:
                continue
            try:
                new_segment = self._parameterize_segment(segment)
            except ValueError as ve:
                print(ve)
                return False
            if new_segment:
                new_segments.append(new_segment)
        return ' '.join(new_segments)


class InputDescriptionParameterizor:
    '''
    Parameterizes input description_segments, replacing tokens with STRING_0, VARIABLE_0, etc
    '''
    def __init__(self, description_segments, variables):
        self.segments = description_segments
        self.parsed_segments = []
        self.parameter_map = {}
        for prefix in PARAMETER_PREFIXES:
            self.parameter_map[prefix] = []
        self.variables = variables

    def _classify_segment(self, text):
        is_quoted = _in_quotes(text)
        if is_quoted:
            unwraped = text[1:-1]
        if text in self.variables:
            return P_VARIABLE, text
        if is_quoted and unwraped in self.variables:
            return P_VARIABLE, unwraped
        p_type = _valid_python(text)
        if p_type:
            return p_type, text
        if is_quoted:
            p_type = _valid_python(unwraped)
            if p_type:
                return p_type, unwraped
        if is_quoted:
            return P_STRING, "'{0}'".format(unwraped)
        if _in_brackets(text):
            # Since text is not a variable/list/etc it must be plain text
            return PLAIN_TEXT, text[1:-1]
        # print('Cannont classify the segment: "{0}"'.format(str(text)))
        return False, False

    def _add_parameter(self, parameter_type, value):
        if value not in self.parameter_map[parameter_type]:
            self.parameter_map[parameter_type].append(value)
        p_type_count = len(self.parameter_map[parameter_type])
        if p_type_count > NUMBER_OF_EACH_PARAMETER_TYPE:
            raise ValueError('Too many parameters of type {0}.'.format(parameter_type))
        return new_parameter_key(parameter_type, p_type_count - 1)

    def _parameterize_quote_bracket_segment(self, segment):
        segment_class, segment = self._classify_segment(segment)
        if segment_class == PLAIN_TEXT:
            return self._parameterize_plain_text_segment(segment)
        if segment_class is False:
            raise Exception('Cannont classify the segment: "{0}"'.format(str(segment)))
        param_key = self._add_parameter(segment_class, segment)
        return param_key

    def _parameterize_plain_text_segment(self, segment):
        new_tokens = []
        tokens = segment.split(' ')
        # tokens = self._replace_worded_numbers(tokens)
        for token in tokens:
            if not token:
                continue
            if token[-1] == ',':
                token = token[:-1]
                if not token:
                    continue
            segment_class, possible_token = self._classify_segment(token)
            if segment_class == PLAIN_TEXT or segment_class is False:
                token = _normalize_string(token)
            else:
                token = self._add_parameter(segment_class, possible_token)
            new_tokens.append(token)
        return ' '.join(new_tokens)

    @staticmethod
    def _replace_worded_numbers(tokens):
        # only works for numbers 0-20
        new_tokens = []
        for token in tokens:
            if token in NUMBERS:
                new_tokens.append(
                    str(NUMBERS.index(token))
                )
            else:
                new_tokens.append(token)
        return new_tokens

    def _parameterize_segment(self, segment):
        if _in_quotes(segment) or _in_brackets(segment):
            return self._parameterize_quote_bracket_segment(segment)
        return self._parameterize_plain_text_segment(segment)

    def parameterize(self):
        new_segments = []
        for segment in self.segments:
            segment = segment.strip()
            if not segment:
                continue
            try:
                new_segment = self._parameterize_segment(segment)
            except ValueError as ve:
                print(ve)
                return False
            if new_segment:
                new_segments.append(new_segment)
        return ' '.join(new_segments)


def new_parameter_key(prefix, i):
    return prefix + '_' + str(i)


def potential_parameter_keys():
    keys = set()
    for prefix in PARAMETER_PREFIXES:
        for i in range(NUMBER_OF_EACH_PARAMETER_TYPE):
            keys.add(
                new_parameter_key(prefix, i)
            )
    return keys


def _valid_python(text):
    try:
        leaf_nodes = _get_leaf_nodes(text)
    except Exception as _:
        return False
    if len(leaf_nodes) > 1:
        for node in leaf_nodes:
            if isinstance(node, (ast.Load)):
                return P_KEY
        print('Weird value check: ' + text)
        return False
    elif not leaf_nodes or type(leaf_nodes[0]) not in AST_TO_PARAMETER_TYPE:
        return False
    return AST_TO_PARAMETER_TYPE[type(leaf_nodes[0])]


def _leaf_nodes(node, stop_at):
    if isinstance(node, stop_at):
        return [node]
    leaves = []
    children = list(ast.iter_child_nodes(node))
    if not children or isinstance(node, (ast.Name)):# , ast.Attribute
        return []
    for child in children:
        leaves += _leaf_nodes(child, stop_at)
    return leaves


def _get_leaf_nodes(code_text, stop_at=LEAF_TYPES):
    root = ast.parse(code_text)
    return _leaf_nodes(root, stop_at)


def _get_code_info(code):
    variables = set()
    strings = set()
    leaf_nodes = _get_leaf_nodes(code, stop_at=(ast.Str, ast.Name))
    for node in leaf_nodes:
        if isinstance(node, (ast.Str)):
            strings.add(node.s)
        elif isinstance(node, (ast.Name)):
            if not isinstance(node.ctx, (ast.Load)) or node.id not in BUILTIN_METHODS:
                variables.add(node.id)
    return variables, strings


def parse_a_text_pair(text_pair):
    description = text_pair[DESCRIPTION_INDEX]
    code = text_pair[CODE_INDEX]

    segmentor = Segmentor(description)
    segments = segmentor.find_segments()
    try:
        variables, strings = _get_code_info(code)
    except Exception:
        variables, strings = [], []
    parameterizor = DescriptionCodePairParameterizor(segments, code, variables, strings)
    new_description = parameterizor.parameterize()
    if not new_description:
        return False

    result = [None, None]
    result[DESCRIPTION_INDEX] = new_description
    result[CODE_INDEX] = parameterizor.new_code
    return result


def parse_saved_pairs(dict_pairs):
    '''
    From a list of description-code pairs in a dictionary,
    Tokenize and parameterize each (not needing to remember what each parameter is).
    '''
    parsed_pairs = []
    for pair_dict in dict_pairs:
        pair_parsed = _parse_a_text_pair(pair_dict)
        if pair_parsed:
            parsed_pairs.append(pair_parsed)
    return parsed_pairs


def parse_input_description(description, variables):
    segmentor = Segmentor(description)
    segments = segmentor.find_segments()
    parameterizor = InputDescriptionParameterizor(segments, variables)
    new_description = parameterizor.parameterize()
    if not new_description:
        return False, False
    parameter_replacement_map = _replacement_map_from_parameter_map(parameterizor.parameter_map)
    return parameter_replacement_map, new_description


def _replacement_map_from_parameter_map(parameter_map):
    replacement_map = {}
    for prefix, parameter_set in parameter_map.items():
        for i, val in enumerate(list(parameter_set)):
            replacement_map[new_parameter_key(prefix, i)] = val
    return replacement_map

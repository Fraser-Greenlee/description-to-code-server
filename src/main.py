'''
Main script where everything is ran from.
'''
from random import randint
from argparse import ArgumentParser
import torch

from _load_pickle import load_pickle
from _refresh_language_instances import new_language_instances, save_languages
from _evaluate import Evaluator
from _encoder import EncoderLSTM
from _decoder import AttnDecoderLSTM
from _train import Train
from _find_pair_tokens import parse_input_description
from _refresh_training_data import refresh_training_data
from _handle_models import new_model_path, archive_current_models
from _constants import (
    HIDDEN_SIZE,
    MODEL,
    TRAINING_EPOCHS,
    LANGUAGE_INSTANCES_PATH,
    DATASET_SPLIT_MAX_VALUE,
    MIN_TUNE_ACCURACY
)


def _new_device():
    return torch.device("cuda" if torch.cuda.is_available() else "cpu")


def refresh_language_instances():
    description_lang, code_lang = new_language_instances()
    save_languages(description_lang, code_lang)


def train(use_train_test_data=False):
    'Train a new model, saving the trained encoder and decoder.'
    description_lang, code_lang = new_language_instances()
    refresh_training_data(description_lang, code_lang)
    device = _new_device()
    encoder = EncoderLSTM(device, description_lang.n_tokens, HIDDEN_SIZE).to(device)
    decoder = AttnDecoderLSTM(device, HIDDEN_SIZE, code_lang.n_tokens, dropout_p=0.1).to(device)
    trainer = Train(device, encoder, decoder)
    if use_train_test_data:
        raise Exception('Not Implimented Yet')
    else:
        trainer.train_epochs(TRAINING_EPOCHS)
    save_languages(description_lang, code_lang)


def _setup():
    device = _new_device()
    description_lang, code_lang = load_pickle(LANGUAGE_INSTANCES_PATH)
    encoder, decoder = torch.load(MODEL)
    encoder.eval()
    decoder.eval()
    evaluator = Evaluator(device, description_lang, code_lang, encoder, decoder)
    return device, description_lang, code_lang, encoder, decoder, evaluator


def evaluate(use_train_test_data=False, in_use_model=False):
    'Evaluate the saved model at MODEL.'
    _, _, _, _, _, evaluator = _setup()
    if use_train_test_data or in_use_model:
        raise Exception('Not Implimented Yet')
    evaluator.evaluate_randomly()
    evaluator.show_accuracy()


def tune_to_new_data(new_tensors):
    'Tune the model to the tensors of corrected predictions.'
    device, _, _, encoder, decoder, evaluator = _setup()
    trainer = Train(device, encoder, decoder)
    passed = trainer.tune_model(new_tensors)
    if not passed:
        raise RuntimeError('Training failed.')
    total_count, correct_count = evaluator.accuracy_on_split_index(randint(0, DATASET_SPLIT_MAX_VALUE - 1))
    accuracy = 100*(correct_count/total_count)
    if accuracy < MIN_TUNE_ACCURACY:
        total_count, correct_count = evaluator.accuracy_on_split_index(randint(0, DATASET_SPLIT_MAX_VALUE - 1))
        accuracy = 100*(correct_count/total_count)
        if accuracy < MIN_TUNE_ACCURACY:
            raise RuntimeError('Model accuracy decreased too much.')
    torch.save([encoder, decoder], new_model_path(trained=False))


def interactive():
    'Run the saved model from the command-line.'
    _, _, _, _, _, evaluator = _setup()
    while True:
        # variables = input('Available variables (comma seperated): ').split(',')
        raw_description = input('Input your description: ')
        if not raw_description:
            continue

        parameter_replacement_map, parameterized_description = parse_input_description(raw_description, [])

        is_unsure, parameterized_code_tokens = evaluator.evaluate_handling_tensors(parameterized_description)
        if is_unsure:
            print('Unsure, try another description.')
            continue

        code = evaluator.replace_code_using_map(parameterized_code_tokens, parameter_replacement_map)

        if code is False:
            print('Code not generated.')
        else:
            print('< ' + code)


def _get_arguments():
    parser = ArgumentParser()
    parser.add_argument('-m', '--mode', default='train', help='Mode to run the script with.')
    return parser.parse_args()


if __name__ == '__main__':
    ARGS = _get_arguments()
    if ARGS.mode == 'train':
        train()
    elif ARGS.mode == 'evaluate':
        evaluate()
    elif ARGS.mode == 'interactive':
        interactive()
    elif ARGS.mode == 'refresh_language_instances':
        refresh_language_instances()
    else:
        raise Exception('No valid mode argument.')

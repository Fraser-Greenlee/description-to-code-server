import tokenize as tk
BOM_UTF8 = b'\xef\xbb\xbf'


def _new_fake_lines_iterable(string):
    bytes_string = bytes(string, 'utf-8')
    return iter([BOM_UTF8, bytes_string])


def raw_tokenize(snippet):
    tokens = []
    fake_iter = _new_fake_lines_iterable(snippet)
    for token_object in tk.tokenize(fake_iter.__next__):
        tokens.append(token_object.string)
    return tokens[1:-1]

def tokenize(string):
    'Tokenize a string of description OR code and return the tokens.'
    if type(string) is list:
        raise Exception('Tokenizing already tokenized string!')
    bad_tokens = raw_tokenize(string)
    last_token = None
    true_tokens = []
    for token in bad_tokens:
        if token == '':
            continue
        if token == '-':
            last_token = '-'
            continue
        if last_token is not None:
            token = last_token + token
            last_token = None
        true_tokens.append(token)
    return true_tokens

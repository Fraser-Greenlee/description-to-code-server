import pymongo


DB_CLIENT = pymongo.MongoClient("mongodb://localhost:27017/")
DB = DB_CLIENT['description_to_code']

EXPANDED_PAIRS = DB['expanded_pairs']
BASIC_PAIRS = DB['basic_pairs']
GENERATED_IN_HOUSE = DB['generated_in_house']
GENERATED_IN_HOUSE_SEARCHABLE = DB['generated_in_house_searchable']
DISTINCT_PAIRS = DB['compiled_pairs_distinct_description']
CORRECTLY_GENERATED_PAIRS = DB['correctly_generated_pairs']
INCORRECTLY_GENERATED_PAIRS = DB['incorrectly_generated_pairs']

MAIN_PAIRS = GENERATED_IN_HOUSE

PREDICTIONS = DB['predictions']
PREDICTIONS_ARCHIVE = DB['predictions_archive']

NEW_USER_TOKENS = DB['new_user_tokens']
TOKENS = DB['tokens']

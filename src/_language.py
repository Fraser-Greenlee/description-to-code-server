'''
Holds Language classes used to keep track of token indeces.

Modified from tutorial https://pytorch.org/tutorials/intermediate/seq2seq_translation_tutorial.html
'''
from _tokenize_python import py_tokenize
from _find_pair_tokens import new_parameter_key
from _constants import (
    SOS_TOKEN,
    EOS_TOKEN,
    PARAMETER_PREFIXES,
    NUMBER_OF_EACH_PARAMETER_TYPE
)


class Description:
    def __init__(self):
        self.token2index = {}
        self.token2count = {}
        self.index2token = {
            SOS_TOKEN: "SOS",
            EOS_TOKEN: "EOS"
        }
        self.n_tokens = len(self.index2token)
        self._add_parameter_tokens()

    def _add_parameter_tokens(self):
        prefixes = PARAMETER_PREFIXES
        for prefix in prefixes:
            for i in range(NUMBER_OF_EACH_PARAMETER_TYPE):
                self.add_token(new_parameter_key(prefix, i))

    def add_sentence(self, line):
        tokens = self.tokenize(line)
        self.add_tokens(tokens)

    def add_tokens(self, tokens):
        for token in tokens:
            self.add_token(token)

    def add_token(self, token):
        if token not in self.token2index:
            self.token2index[token] = self.n_tokens
            self.token2count[token] = 1
            self.index2token[self.n_tokens] = token
            self.n_tokens += 1
        else:
            self.token2count[token] += 1

    @staticmethod
    def tokenize(snippet):
        return snippet.split(' ')


class Code:
    def __init__(self):
        self.token2index = {}
        self.token2count = {}
        self.index2token = {
            SOS_TOKEN: "SOS",
            EOS_TOKEN: "EOS"
        }
        self.n_tokens = len(self.index2token)
        self._add_parameter_tokens()

    def _add_parameter_tokens(self):
        prefixes = PARAMETER_PREFIXES
        for prefix in prefixes:
            for i in range(NUMBER_OF_EACH_PARAMETER_TYPE):
                self.add_token(new_parameter_key(prefix, i))

    def add_sentence(self, line):
        tokens = self.tokenize(line)
        self.add_tokens(tokens)

    def add_tokens(self, tokens):
        for token in tokens:
            self.add_token(token)

    def add_token(self, token):
        if token not in self.token2index:
            self.token2index[token] = self.n_tokens
            self.token2count[token] = 1
            self.index2token[self.n_tokens] = token
            self.n_tokens += 1
        else:
            self.token2count[token] += 1

    @staticmethod
    def tokenize(snippet):
        return py_tokenize(snippet)

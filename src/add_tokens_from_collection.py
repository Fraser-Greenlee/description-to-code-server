import datetime
from tqdm import tqdm

from _load_pickle import load_pickle
from _db import GENERATED_IN_HOUSE, TOKENS
from _constants import (
    LANGUAGE_INSTANCES_PATH,
    DESCRIPTION_KEY,
    CODE_KEY
)

PAIRS_COLLECTION = GENERATED_IN_HOUSE
ORIGINAL = True


def _new_tokens(language, token_list):
    return [
        {
            'token': tk,
            'language': language,
            'original': ORIGINAL,
            'made_at_utc': datetime.datetime.now()
        } for tk in token_list
    ]


def _collection_tokens():
    print('Getting all tokens from the collection.')
    pbar = tqdm(total=PAIRS_COLLECTION.find({}).count())
    tokens = []
    description_lang, code_lang = load_pickle(LANGUAGE_INSTANCES_PATH)
    for pair in PAIRS_COLLECTION.find({}):
        desc_tokens = description_lang.tokenize(pair[DESCRIPTION_KEY])
        code_tokens = code_lang.tokenize(pair[CODE_KEY])
        tokens += _new_tokens(DESCRIPTION_KEY, desc_tokens) + _new_tokens(CODE_KEY, code_tokens)
        pbar.update(1)
    pbar.close()
    return tokens


def _find_new_tokens(tokens):
    print('Adding all the new tokens to the TOKENS collection.')
    token_sets = {
        DESCRIPTION_KEY: set(),
        CODE_KEY: set()
    }
    new_tokens = []
    for token_doc in tqdm(tokens):
        if token_doc['token'] not in token_sets[token_doc['language']] and TOKENS.find({'token': token_doc['token'], 'language': token_doc['language']}).count() == 0:
            new_tokens.append(token_doc)
            token_sets[token_doc['language']].add(token_doc['token'])
    return new_tokens


def _add_new_tokens(tokens):
    new_tokens = _find_new_tokens(tokens)
    if new_tokens:
        TOKENS.insert_many(new_tokens)
    print('Inserted', len(new_tokens), 'new tokens')


def add_tokens_from_collection():
    collection_tokens = _collection_tokens()
    _add_new_tokens(collection_tokens)


if __name__ == '__main__':
    add_tokens_from_collection()

import time
import math


def _as_minutes(seconds):
    minutes = math.floor(seconds / 60)
    seconds -= minutes * 60
    return '%dm %ds' % (minutes, seconds)


def time_since(since, percent):
    now = time.time()
    seconds = now - since
    estimated_seconds = seconds / (percent)
    remaining_seconds = estimated_seconds - seconds
    return '%s (- %s)' % (_as_minutes(seconds), _as_minutes(remaining_seconds))

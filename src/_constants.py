
COLAB_ALL_ANNOTATIONS = 'drive/My Drive/ML Data/description-to-code/my-annotations/all.annotations.pickle'
ALL_COMPILED_ANNOTATIONS = 'data/all.compiled.annotations.json'
TRAIN_COMPILED_ANNOTATIONS = 'data/train.compiled.annotations.json'
TEST_COMPILED_ANNOTATIONS = 'data/test.compiled.annotations.json'
SOURCES_FOLDER = 'data/sources/'
DATA_FOLDER = 'data/'
STATE_FOLDER = 'starting_state/'
LANGUAGE_INSTANCES_PATH = STATE_FOLDER + 'descripion_code.pickle'
COMMON_WORDS_PATH = STATE_FOLDER + 'description/common_words.json'
DESC_SYN_FOLDER = STATE_FOLDER + 'description/synonyms/'
SYNONYMS_PATH = DESC_SYN_FOLDER + 'dict.json'
SYNONYM_SETS_PATH = DESC_SYN_FOLDER + 'sets.json'
SYNONYMS_TO_BE_ADDED = DESC_SYN_FOLDER + 'to_be_added.json'

MODEL = 'models/epochs/2.pt' # 'model.pt'
MODEL_IN_USE_FOLDER = 'models/in_use/'
MODEL_ARCHIVE_FOLDER = MODEL_IN_USE_FOLDER + 'archive/'
MODEL_EPOCH_TEMPLATE = 'models/epochs/{0}.pt'

DATASET_SPLIT_FIELD = 'split_field'
DATASET_SPLIT_MAX_VALUE = 99

SEARCH_RESULTS_LIMIT = 10
SEARCH_RESULTS_FIND_LIMIT = 100

HIDDEN_SIZE = 256

SOS_TOKEN = 0
EOS_TOKEN = 1
MAX_STRING_LENGTH = 60

TRAINING_EPOCHS = 3 # 39
TEACHER_FORCING_RATIO = 0.5

MIN_TUNE_ACCURACY = 90

P_STRING = 'STRING'
P_NUMBER = 'NUMBER'
P_LIST = 'LIST'
P_DICT = 'DICT'
P_SET = 'SET'
P_TUPLE = 'TUPLE'
P_VARIABLE = 'VARIABLE'
P_KEY = 'KEY'
PARAMETER_PREFIXES = [
    P_STRING,
    P_NUMBER,
    P_LIST,
    P_DICT,
    P_SET,
    P_TUPLE,
    P_VARIABLE,
    P_KEY
]
NUMBER_OF_EACH_PARAMETER_TYPE = 5
PARAMETER_KEYS = set()
for prefix in PARAMETER_PREFIXES:
    for i in range(NUMBER_OF_EACH_PARAMETER_TYPE):
        PARAMETER_KEYS.add(
            prefix + '_' + str(i)
        )

DESCRIPTION_INDEX = 0
CODE_INDEX = 1
VARIABLE_INDEX = 2

DESCRIPTION_KEY = 'description'
CODE_KEY = 'code'
MODULES_KEY = 'modules'
ADDED_AT_KEY = 'added_at'

PORT = '4444'

SAVE_RESULTS = False

MAX_PERMUTATIONS_PER_SAMPLE = 50
MAX_PERMUTATIONS_RESAMPLING = 1000

NUMBERS = 'zero one two three four five six seven eight nine ten eleven twelve thirteen fourteen fithteen sixteen eighteen nineteen twenty'.split()

import random
import torch
from tqdm import tqdm

from _constants import (
    MAX_STRING_LENGTH,
    SOS_TOKEN,
    EOS_TOKEN,
    DESCRIPTION_INDEX,
    CODE_INDEX,
    DATASET_SPLIT_MAX_VALUE
)
from _find_pair_tokens import potential_parameter_keys
from _make_tensor import tensors_from_indeces_file, tokens_from_tensor, string_from_tensor, tensor_from_string
from _tokenize_python import raw_tokenize, join_code_tokens


class Evaluator:
    def __init__(self, device, description_lang, code_lang, encoder, decoder):
        self.device = device
        self.description_lang = description_lang
        self.code_lang = code_lang
        self.encoder = encoder
        self.decoder = decoder
        self.potential_parameter_keys = potential_parameter_keys()


    def evaluate(self, input_tensor, max_length=MAX_STRING_LENGTH):
        '''Use the model to predict the code for a given description and return the decoder attentions.'''
        with torch.no_grad():
            input_length = input_tensor.size()[0]
            encoder_hidden = self.encoder.init_hidden()

            encoder_outputs = torch.zeros(max_length, self.encoder.hidden_size, device=self.device)

            for encoder_i in range(input_length):
                encoder_output, encoder_hidden = self.encoder(input_tensor[encoder_i], encoder_hidden)
                encoder_outputs[encoder_i] += encoder_output[0, 0]

            decoder_input = torch.tensor([[SOS_TOKEN]], device=self.device)  # SOS

            decoder_hidden = encoder_hidden

            decoded_indeces = []
            sureness_values = []
            decoder_attentions = torch.zeros(max_length, max_length)

            for decoder_i in range(max_length):
                decoder_output, decoder_hidden, decoder_attention = self.decoder(decoder_input, decoder_hidden, encoder_outputs)
                decoder_attentions[decoder_i] = decoder_attention.data
                sureness, topi = decoder_output.data.topk(1)
                sureness_values.append(sureness)
                decoded_indeces.append(topi.item())
                if topi.item() == EOS_TOKEN:
                    break
                decoder_input = topi.squeeze().detach()

        return decoded_indeces, sureness_values, decoder_attentions[:decoder_i + 1]


    def evaluate_randomly(self, number_of_examples=10):
        '''Evaluate and print the results of the model predicting some random pairs.'''
        tensors = tensors_from_indeces_file(self.device, random.randint(0, DATASET_SPLIT_MAX_VALUE))
        for _ in range(number_of_examples):
            input_pair = random.choice(tensors)
            print('>', string_from_tensor(self.description_lang, input_pair[DESCRIPTION_INDEX]))
            print('=', string_from_tensor(self.code_lang, input_pair[CODE_INDEX]))
            output_indeces = self.evaluate(input_pair[DESCRIPTION_INDEX])[0]
            output_code = string_from_tensor(self.code_lang, output_indeces)
            print('<', output_code)
            print('')


    def accuracy_on_split_index(self, split_index):
        total_count, correct_count = 0, 0
        tensors = tensors_from_indeces_file(self.device, split_index)
        for pair in tensors:
            decoded_indeces = self.evaluate(pair[DESCRIPTION_INDEX])[0]
            true_indeces = [int(i) for i in pair[CODE_INDEX]]
            if decoded_indeces == true_indeces:
                correct_count += 1
            total_count += 1
        return total_count, correct_count


    def full_dataset_accuracy(self):
        '''Measure the accuracy of the model on all pairs in the index files.'''
        total_count, correct_count = 0, 0
        for split_index in tqdm(range(DATASET_SPLIT_MAX_VALUE)):
            add_total_count, add_correct_count = self.accuracy_on_split_index(split_index)
            total_count += add_total_count
            correct_count += add_correct_count
            print('Accuracy:', 100*(correct_count/total_count))
        accuracy = 100*(correct_count/total_count)
        return accuracy, total_count, correct_count


    def show_accuracy(self):
        '''Show the accuracy of the model on the given pairs.'''
        accuracy, total_count, correct_count = self.full_dataset_accuracy()
        print('Accuracy: {0}%, ({1}/{2})'.format(accuracy, correct_count, total_count))


    def accuracy_on_pairs(self, tensor_pairs):
        '''Measure the accuracy on given list of pairs of tensors'''
        correct_count = 0
        total_count = 0
        for pair in tensor_pairs:
            decoded_indeces = self.evaluate(pair[DESCRIPTION_INDEX])[0]
            true_indeces = [int(i) for i in pair[CODE_INDEX]]
            if decoded_indeces == true_indeces:
                correct_count += 1
            total_count += 1
        accuracy = 100*(correct_count/total_count)
        return accuracy


    def evaluate_and_show_attention(self, description_tokens):
        '''
        Evaluate a description.
        NOTE: Does not show the attention right now, had some issues with the plotting library.
        '''
        code_tokens = self.evaluate(description_tokens)[0]
        print('input =', description_tokens)
        print('output =', ' '.join(code_tokens))


    @staticmethod
    def unsure(sureness_values):
        min_sureness = float(min(sureness_values))
        print('min sureness', min_sureness)
        return min_sureness < -1.5


    def evaluate_handling_tensors(self, description):
        '''
        Take a description string and return code tokens.
        '''
        description_tensor = tensor_from_string(self.device, self.description_lang, description)
        code_indeces, sureness_values, _ = self.evaluate(description_tensor)
        is_unsure = self.unsure(sureness_values)
        print('is_unsure:', is_unsure)
        code_tokens = tokens_from_tensor(self.code_lang, code_indeces)
        return is_unsure, code_tokens


    def replace_code_using_map(self, raw_code_tokens, parameter_replacement_map, remove_EOS=True):
        '''
        Convert raw code tokens to parameterized code given a parameter replacement map.
        '''
        final_tokens = []
        for token in raw_code_tokens:
            if token in parameter_replacement_map:
                final_tokens.append(
                    parameter_replacement_map[token]
                )
            elif token in self.potential_parameter_keys:
                print('Outputed a parameter that was not in the description.')
                return False
            else:
                final_tokens.append(token)
        if remove_EOS:
            final_tokens = final_tokens[:-1]
        try:
            raw_tokenize(''.join(final_tokens))
        except Exception as _:
            print(final_tokens)
            return False
        return join_code_tokens(final_tokens)

from flask import Flask, request, jsonify
fake_server = Flask(__name__)

@fake_server.route('/get_code', methods=['POST'])
def get_code():
    print('REQUEST')
    print(request.json)
    return jsonify({
        'id': 1,
        'code_options': ['code a', 'code b']
    })


@fake_server.route('/save_prediction_response', methods=['POST'])
def save_prediction_response():
    print('PREDICTION RESPONSE')
    print(request.json)
    return 'recieved prediction response'

import unittest

from _handle_models import archive_current_models, _model_files
from _constants import MODEL_IN_USE_FOLDER

class TestArchiveCurrentModels(unittest.TestCase):

    def test_archive_current_models(self):
        '''
        This will ACTUALLY archive the models so be careful when running.
        '''
        archive_current_models()
        self.assertEqual(
            len(_model_files()),
            0
        )


if __name__ == '__main__':
    unittest.main()

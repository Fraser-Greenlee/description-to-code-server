'''
Model Encoder

Modified from tutorial https://pytorch.org/tutorials/intermediate/seq2seq_translation_tutorial.html
'''
import torch
from torch import nn


class EncoderLSTM(nn.Module):
    def __init__(self, device, input_size, hidden_size):
        self.device = device
        super(EncoderLSTM, self).__init__()
        self.hidden_size = hidden_size

        self.embedding = nn.Embedding(input_size, hidden_size)
        self.lstm = nn.LSTM(hidden_size, hidden_size)

    def forward(self, a_input, hidden):
        embedded = self.embedding(a_input).view(1, 1, -1)
        output = embedded
        output, hidden = self.lstm(output, hidden)
        return output, hidden

    def init_hidden(self):
        hidden_states = torch.zeros(1, 1, self.hidden_size, device=self.device)
        cell_states = torch.zeros(1, 1, self.hidden_size, device=self.device)
        return (hidden_states, cell_states)

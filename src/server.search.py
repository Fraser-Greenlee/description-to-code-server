import datetime
import json
import os
from flask import Flask, request, jsonify
from bson import ObjectId

from _constants import (
    PORT,
    LANGUAGE_INSTANCES_PATH,
    SEARCH_RESULTS_LIMIT,
    SEARCH_RESULTS_FIND_LIMIT
)
from _db import (
    PREDICTIONS,
    GENERATED_IN_HOUSE_SEARCHABLE
)
from _find_pair_tokens import parse_input_description, parse_a_text_pair
from _load_pickle import load_pickle
from _evaluate import Evaluator
DESCRIPTION_LANG, CODE_LANG = load_pickle(LANGUAGE_INSTANCES_PATH)
EVALUATOR = Evaluator(None, DESCRIPTION_LANG, CODE_LANG, None, None)
SERVER = Flask(__name__)


def _parse_get_code(request_json):
    raw_description = request_json['description']
    if not raw_description:
        raise RuntimeError('empty description')

    variables = []
    parameter_replacement_map, parameterized_description = parse_input_description(raw_description, variables)

    matching_pairs = list(
        GENERATED_IN_HOUSE_SEARCHABLE.find(
            {'$text': {
                '$search': "\"{0}\"".format(parameterized_description)
            }},
            {'score': {
                '$meta': "textScore"
            }}
        ).sort([
            ('score', {'$meta': 'textScore'})
        ]).limit(SEARCH_RESULTS_FIND_LIMIT)
    )
    if not matching_pairs:
        matching_pairs = list(
            GENERATED_IN_HOUSE_SEARCHABLE.find(
                {'$text': {
                    '$search': "{0}".format(parameterized_description)
                }},
                {'score': {
                    '$meta': "textScore"
                }}
            ).sort([
                ('score', {'$meta': 'textScore'})
            ]).limit(SEARCH_RESULTS_FIND_LIMIT)
        )

    code_options = []
    parameterized_code_options = []
    for pair in matching_pairs:
        parameterized_code_tokens = CODE_LANG.tokenize(pair['code'])
        raw_code = EVALUATOR.replace_code_using_map(parameterized_code_tokens, parameter_replacement_map, remove_EOS=False)
        if raw_code is not False:
            parameterized_code_options.append(pair['code'])
            code_options.append(raw_code)

    return parameterized_description, raw_description, parameterized_code_options[:SEARCH_RESULTS_LIMIT], code_options[:SEARCH_RESULTS_LIMIT]


def _save_initial_prediction(parameterized_description, raw_description, parameterized_code_options, code_options):
    row = {
        'description': {
            'parameterized': parameterized_description,
            'raw': raw_description
        },
        'code': {
            'generated': {
                'parameterized': parameterized_code_options,
                'raw': code_options
            },
            'confirmed': {
                'parameterized': None,
                'raw': None
            }
        },
        'context': {
            'modules': [],
            'variables': []
        },
        'got_response': False,
        'attempted_to_train_on': False,
        'added_at': datetime.datetime.now(),
        'result_type': 'search'
    }
    inserted_response = PREDICTIONS.insert_one(row)
    return str(inserted_response.inserted_id)


@SERVER.route('/get_code', methods=['POST'])
def get_code():
    try:
        parameterized_description, raw_description, parameterized_code_options, code_options = _parse_get_code(request.json)
    except RuntimeError as error:
        return jsonify({
            'error': str(error),
            'message': 'Bad input, try another description.'
        })

    prediction_id = _save_initial_prediction(
        parameterized_description, raw_description, parameterized_code_options, code_options
    )
    return jsonify({
        'code_options': code_options,
        'prediction_id': prediction_id
    })


def _prediction_response_params(request_json):
    prediction_id = request_json['prediction_id']
    prediction = PREDICTIONS.find_one({'_id': ObjectId(prediction_id)})
    raw_code = request_json['code']
    return prediction_id, prediction, raw_code


def _update_prediction(prediction_id, prediction):
    PREDICTIONS.update_one(
        {'_id': ObjectId(prediction_id)},
        {
            '$set': {
                'description': prediction['description'],
                'code': prediction['code'],
                'got_response': True
            }
        }
    )


@SERVER.route('/save_prediction_response', methods=['POST'])
def save_prediction_response():
    prediction_id, prediction, raw_code = _prediction_response_params(request.json)
    prediction['code']['confirmed']['raw'] = raw_code
    _update_prediction(prediction_id, prediction)
    return jsonify({
        'completed': True
    })


@SERVER.route('/save_object', methods=['POST'])
def save_object():
    ids = []
    for file_name in os.listdir('user_study_keylogs/'):
        if file_name[0] != '.':
            ids.append(int(file_name[:-5]))
    max_id = sorted(ids)[-1]
    new_id = max_id + 1
    data = request.json
    with open('user_study_keylogs/' + str(new_id) + '.json', 'w') as outfile:
        json.dump(data, outfile)
    return ''


if __name__ == "__main__":
    SERVER.run(port=PORT)

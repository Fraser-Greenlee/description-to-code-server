import datetime

from _db import TOKENS


def new_token(language, token):
    return {
        'token': token,
        'language': language,
        'original': False,
        'confirmed': False,
        'made_at_utc': datetime.datetime.utcnow()
    }

def insert_new_tokens(language, tokens):
    rows = [
        new_token(language, token) for token in tokens
    ]
    TOKENS.insert_many(rows)

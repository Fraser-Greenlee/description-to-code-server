from tqdm import tqdm

from _db import GENERATED_IN_HOUSE
from _constants import (
    DATASET_SPLIT_FIELD,
    DATASET_SPLIT_MAX_VALUE
)
PAIRS_COLLECTION = GENERATED_IN_HOUSE
FIND_QUERY = {} # {'split_field': { "$exists" : False}}


def assign_split_fields():
    pbar = tqdm(total=PAIRS_COLLECTION.find(FIND_QUERY).count())
    pairs = PAIRS_COLLECTION.find(FIND_QUERY)
    i = 0
    for pair in pairs:
        PAIRS_COLLECTION.update(
            {'_id': pair['_id']},
            {'$set': {DATASET_SPLIT_FIELD: i}}
        )
        i += 1
        if i >= DATASET_SPLIT_MAX_VALUE:
            pbar.update(DATASET_SPLIT_MAX_VALUE)
            i = 0
    pbar.close()


if __name__ == '__main__':
    assign_split_fields()

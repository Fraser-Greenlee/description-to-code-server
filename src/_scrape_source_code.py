import ast

from _built_in_methods import BUILTIN_METHODS

LEAF_TYPES = (
    ast.UnaryOp,
    ast.List,
    ast.Dict,
    ast.Tuple,
    ast.Set,
    ast.Str,
    ast.Num,
    ast.Load
)


def variables_from_source_code(source_code, line_number):
    '''
    Given a string of source_code and a line_number, return the names of the relevent global & local variables.
    '''
    lines = source_code.split('\n')
    function_start = _find_function_start(lines, line_number)
    variables = set()
    if function_start is not None:
        variables.update(_function_variables(lines, function_start))
    variables.update(_global_variables(lines))
    # remove datetime as a variable
    if 'datetime' in variables:
        variables.remove('datetime')
    return list(variables)


def _find_function_start(lines, line_number):
    i = line_number
    in_quotes = False
    while i >= 0 and in_quotes or (lines[i] and lines[i][0] == ' ' and lines[i].lstrip()[:4] != 'def '):
        if "'''" in lines[i] or '"""' in lines[i]:
            if in_quotes:
                in_quotes = False
            else:
                in_quotes = True
        i -= 1
    if lines[i].lstrip()[:4] == 'def ':
        return i
    return None


def _find_function_end(lines, function_start):
    i = function_start + 1
    in_quotes = False
    while i >= 0 and in_quotes or (lines[i] and lines[i][0] == ' ' and lines[i].lstrip()[:4] != 'def '):
        if "'''" in lines[i] or '"""' in lines[i]:
            if in_quotes:
                in_quotes = False
            else:
                in_quotes = True
        i += 1
    return i


def _function_variables(lines, function_start):
    function_end = _find_function_end(lines, function_start)
    variables = _variables_in_function_lines(lines[function_start:function_end])
    return variables


def _variables_in_function_lines(lines):
    code = _remove_tab(lines[1:])
    param_variables = _get_function_params(lines[0])
    try:
        code_variables = _get_code_variables('\n'.join(code))
    except Exception:
        code_variables = set()
    param_variables.update(code_variables)
    return param_variables


def _remove_tab(lines):
    space_count = 0
    for space_count, char in enumerate(lines[0]):
        if char != ' ':
            break
    new_lines = []
    for line in lines:
        new_lines.append(
            line[space_count:]
        )
    return new_lines


def _get_function_params(function_def):
    mini_function = '''{0}
    print('x')'''.format(function_def)
    params = set()
    for arg in ast.parse(mini_function).body[0].args.args:
        params.add(arg.arg)
    return params


def _global_variables(lines):
    non_tabbed_lines = _get_non_tabbed_lines(lines)
    return _get_code_variables('\n'.join(non_tabbed_lines))


def _get_non_tabbed_lines(lines):
    non_tabbed_lines = []
    in_function_or_class = False
    for line in lines:
        class_or_def = (line[:4] == 'def ' or line[:6] == 'class ')
        if in_function_or_class and line and line[0] != ' ' and not class_or_def:
            in_function_or_class = False
        if not in_function_or_class and line and not class_or_def:
            non_tabbed_lines.append(line)
        elif not in_function_or_class and class_or_def:
            in_function_or_class = True
    return non_tabbed_lines


def _get_code_variables(code):
    variables = set()
    leaf_nodes = _get_leaf_nodes(code, stop_at=(ast.Name))
    for node in leaf_nodes:
        if isinstance(node, (ast.Name)):
            if not isinstance(node.ctx, (ast.Load)) or node.id not in BUILTIN_METHODS:
                variables.add(node.id)
    return variables


def _get_leaf_nodes(code_text, stop_at=LEAF_TYPES):
    root = ast.parse(code_text)
    return _leaf_nodes(root, stop_at)


def _leaf_nodes(node, stop_at):
    if isinstance(node, stop_at):
        return [node]
    leaves = []
    children = list(ast.iter_child_nodes(node))
    if not children or isinstance(node, (ast.Name)):
        return []
    for child in children:
        leaves += _leaf_nodes(child, stop_at)
    return leaves

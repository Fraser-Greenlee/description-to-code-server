import tokenize as tk
from yapf.yapflib.yapf_api import FormatCode
BOM_UTF8 = b'\xef\xbb\xbf'
QUOTES = '`\'"'

from _constants import (
    PARAMETER_KEYS
)


def _new_fake_lines_iterable(string):
    bytes_string = bytes(string, 'utf-8')
    return iter([BOM_UTF8, bytes_string])


def raw_tokenize(snippet):
    tokens = []
    fake_iter = _new_fake_lines_iterable(snippet)
    for token_object in tk.tokenize(fake_iter.__next__):
        tokens.append(token_object.string)
    return tokens[1:-1]

def py_tokenize(string):
    'Tokenize a string of code and return the tokens.'
    if type(string) is list:
        raise Exception('Tokenizing already tokenized string!')
    bad_tokens = raw_tokenize(string)
    last_token = None
    true_tokens = []
    for token in bad_tokens:
        if token == '':
            continue
        if token == '-':
            last_token = '-'
            continue
        if last_token is not None:
            if last_token == '-' and token in PARAMETER_KEYS:
                # Handles case where subtracting a parameter, stops tokens like "-VARIABLE_0"
                true_tokens += [last_token, token]
                last_token = None
                continue
            token = last_token + token
            last_token = None
        if token[0] in QUOTES and token[-1] in QUOTES:
            true_tokens += list(token)
        else:
            true_tokens.append(token)
    return true_tokens


def join_code_tokens(code_tokens):
    code_tokens = _join_strings_in_code_tokens(code_tokens)
    raw_code_string = _join_code_tokens_handling_newlines(code_tokens)
    try:
        formatted_code = FormatCode(raw_code_string)[0][:-1]
    except Exception:
        return False
    return formatted_code


def _join_strings_in_code_tokens(code_tokens):
    new_tokens = []
    in_string = False
    string_start = None
    for i, token in enumerate(code_tokens):
        if token in """'"`""":
            if in_string:
                in_string = False
                new_tokens.append(
                    ''.join(code_tokens[string_start:i + 1])
                )
                continue
            else:
                in_string = True
                string_start = i
        if not in_string:
            new_tokens.append(token)
    return new_tokens

def _join_code_tokens_handling_newlines(code_tokens):
    basic_join = ' '.join(code_tokens)
    return basic_join.replace(' \n ', '\n')

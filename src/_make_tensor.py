import torch
import simplejson as json

from _volcab_error import VolcabError
from _constants import (
    EOS_TOKEN,
    DESCRIPTION_KEY,
    CODE_KEY,
    DESCRIPTION_INDEX,
    CODE_INDEX
)


def _indexes_from_tokens(lang, tokens):
    out_of_volcab_tokens = []
    indexes = []
    for token in tokens:
        if token in lang.token2index:
            indexes.append(lang.token2index[token])
        else:
            out_of_volcab_tokens.append(token)

    if out_of_volcab_tokens:
        raise VolcabError('Token(s) are not indexed', lang, out_of_volcab_tokens)
    return indexes


def _indeces_from_string(lang, text):
    tokens = lang.tokenize(text)
    indexes = _indexes_from_tokens(lang, tokens)
    indexes.append(EOS_TOKEN)
    return indexes


def tensors_from_indeces_file(device, index):
    indeces = json.load(open('processed_data/indeces.{0}.json'.format(index), 'r'))
    tensors = []
    for i, _ in enumerate(indeces):
        tensors.append(
            [
                torch.tensor(indeces[i][0], dtype=torch.long, device=device).view(-1, 1),
                torch.tensor(indeces[i][1], dtype=torch.long, device=device).view(-1, 1),
            ]
        )
    return tensors


def tensor_from_string(device, lang, text):
    indexes = _indeces_from_string(lang, text)
    return torch.tensor(indexes, dtype=torch.long, device=device).view(-1, 1)


def tokens_from_tensor(lang, tensor):
    tokens = []
    for i in tensor:
        index = int(i)
        tokens.append(
            lang.index2token[index]
        )
    return tokens


def string_from_tensor(lang, tensor):
    tokens = tokens_from_tensor(lang, tensor)
    return ' '.join(tokens)


def tensors_from_pair(device, description_lang, code_lang, pair):
    description_tensor = tensor_from_string(device, description_lang, pair[DESCRIPTION_INDEX])
    code_tensor = tensor_from_string(device, code_lang, pair[CODE_INDEX])
    return (description_tensor, code_tensor)


def indeces_from_pair_dict(input_lang, output_lang, pair_dict):
    input_indeces = _indeces_from_string(input_lang, pair_dict[DESCRIPTION_KEY])
    target_indeces = _indeces_from_string(output_lang, pair_dict[CODE_KEY])
    return (input_indeces, target_indeces)

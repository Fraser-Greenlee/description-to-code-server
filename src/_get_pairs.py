'''
Functions involved in getting description-code pairs from files and prepairing them for use.
'''
from _tokenize_python import tokenize
from _db import all_pairs, predictions
from _constants import (
    MAX_STRING_LENGTH,
    DESCRIPTION_INDEX,
    CODE_INDEX,
    SOURCES_FOLDER,
    ALL_COMPILED_ANNOTATIONS
)


def _filter_pair(pair):
    return (
        len(pair[DESCRIPTION_INDEX].split(' ')) < MAX_STRING_LENGTH and len(tokenize(pair[CODE_INDEX])) < MAX_STRING_LENGTH
    )


def _filter_pairs(pairs):
    print("Read %s pairs" % len(pairs))
    pairs = [pair for pair in pairs if _filter_pair(pair)]
    print("Trimmed to %s description code pairs" % len(pairs))
    return pairs


def _dict_pairs_to_list(dict_pairs):
    new_pairs = []
    for d_pair in dict_pairs:
        modules = ['MODULE_' + module for module in d_pair['modules']]
        new_pairs.append([
            (' '.join(modules) + ' EOM ' + d_pair['description']).strip(),
            d_pair['code']
        ])
    return new_pairs



def _compile_sources():
    raise Exception('Compiled dataset will not be enhanced!!!')


def get_pairs():
    '''
    Get the pairs in the `all_pairs` collection.
    Return the pairs as a list of [description, code] pairs.
    '''
    pairs = _dict_pairs_to_list(all_pairs.find({}))
    pairs = _filter_pairs(pairs)
    return pairs


def get_new_pairs():
    '''
    Get the pairs in the `predictions` collection.
    Return the pairs as a list of [description, code] pairs.
    '''
    dict_pairs = list(predictions.find(
        {'$where': "this.generated_code != this.corrected_code"}
    ).sort('added_at'))
    if not dict_pairs:
        raise Exception('No new pairs to train from.')
    newest_added_at = dict_pairs[0]['added_at']
    pairs = _dict_pairs_to_list(dict_pairs)
    return newest_added_at, pairs


def transfer_new_pairs(before_added_at):
    '''
    Transfer pairs from `predictions` with added_at berfore `before_added_at` to `all_pairs`.
    '''
    prediction_pairs = predictions.find(
        {'$and': [
            {'$where': "this.generated_code != this.corrected_code"},
            {'added_at': {'$lte': before_added_at}}
        ]}
    )
    pairs = [
        {
            'description': prediction['description'],
            'code': prediction['code'],
            'modules': prediction['modules'],
            'added_at': prediction['added_at']
        } for prediction in prediction_pairs
    ]
    all_pairs.insert_many(pairs)
    predictions.delete_many(
        {'$and': [
            {'$where': "this.generated_code != this.corrected_code"},
            {'added_at': {'$lte': before_added_at}}
        ]}
    )

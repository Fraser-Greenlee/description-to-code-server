'''
Refresh the synonym dict file using the synonym set file.
'''
import json

from _constants import (
    SYNONYMS_PATH,
    SYNONYM_SETS_PATH
)


def _check_valid_sets(list_of_lists):
    'Checks lists in list are valid sets, throws exception if any are not'
    for a_list in list_of_lists:
        if len(a_list) != len(set(a_list)):
            raise RuntimeError('list is not set')


def _make_syn_dict(synonym_lists):
    synonym_dict = dict()
    for syn_list in synonym_lists:
        for word in syn_list:
            synonym_dict[word] = syn_list
    return synonym_dict


def refresh():
    synonym_lists = json.load(open(SYNONYM_SETS_PATH, 'r'))
    _check_valid_sets(synonym_lists)
    synonym_dict = _make_syn_dict(synonym_lists)
    json.dump(synonym_dict, open(SYNONYMS_PATH, 'w'))


if __name__ == "__main__":
    refresh()

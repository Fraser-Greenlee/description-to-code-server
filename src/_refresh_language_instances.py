import pickle
import bson

from _db import TOKENS, NEW_USER_TOKENS
from _language import Description, Code
from _constants import (
    LANGUAGE_INSTANCES_PATH,
    DESCRIPTION_KEY,
    CODE_KEY
)


def _get_languages():
    description_lang = Description()
    code_lang = Code()
    return description_lang, code_lang


def _add_new_user_tokens_to_tokens():
    new_tokens = list(NEW_USER_TOKENS.find({}))
    if new_tokens:
        TOKENS.insert_many([
            {'token': tk['token'], 'language': tk['language'], 'original': False, 'confirmed': tk['confirmed'], 'made_at': tk['made_at_utc']} for tk in new_tokens
        ])
        id_strings = [token['_id'] for token in new_tokens]
        NEW_USER_TOKENS.delete_many(
            {'$or': [{'_id': bson.ObjectId(id_string)} for id_string in id_strings]}
        )


def _add_tokens(description_lang, code_lang):

    for row in TOKENS.find({'language': DESCRIPTION_KEY}):
        description_lang.add_token(row['token'])

    for row in TOKENS.find({'language': CODE_KEY}):
        code_lang.add_token(row['token'])

    return description_lang, code_lang


def _show_token_counts(description_lang, code_lang):
    print("Counted tokens:")
    print('Description', description_lang.n_tokens)
    print('Code', code_lang.n_tokens)


def new_language_instances():
    'Get new language instances'
    description_lang, code_lang = _get_languages()
    _add_new_user_tokens_to_tokens()
    description_lang, code_lang = _add_tokens(description_lang, code_lang)
    _show_token_counts(description_lang, code_lang)
    return description_lang, code_lang


def save_languages(description, code):
    'Save given description & code instances'
    pickle_out = open(LANGUAGE_INSTANCES_PATH, "wb")
    pickle.dump((description, code), pickle_out)
    pickle_out.close()

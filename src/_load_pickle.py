'''
Loads an object from a pickle file.
'''
import pickle


def load_pickle(path):
    'load the (description_lang, code_lang) tuple'
    pickle_in = open(path, 'rb')
    obj = pickle.load(pickle_in)
    pickle_in.close()
    return obj

from tqdm import tqdm

from _language import Description, Code
from _new_token import new_token
from _db import GENERATED_IN_HOUSE, TOKENS

PAIRS = GENERATED_IN_HOUSE


def run():
    inserted_count = 0
    pbar = tqdm(total=PAIRS.find({}).count())
    description_lang = Description()
    code_lang = Code()
    pbar.update(int(PAIRS.find({}).count() * 0.6))
    for pair in tqdm(PAIRS.find({}).skip(int(PAIRS.find({}).count() * 0.6)).batch_size(1000)):
        for token in description_lang.tokenize(pair['description']):
            if not TOKENS.find_one({'token': token, 'language': 'description'}):
                TOKENS.insert_one(
                    new_token('description', token)
                )
                inserted_count += 1
        for token in code_lang.tokenize(pair['code']):
            if not TOKENS.find_one({'token': token, 'language': 'code'}):
                TOKENS.insert_one(
                    new_token('code', token)
                )
                inserted_count += 1
        pbar.update(1)
    pbar.close()
    print('inserted', inserted_count)
    print('')


if __name__ == '__main__':
    run()

from shutil import copyfile
import os
import time

from _constants import (
    MODEL_IN_USE_FOLDER,
    MODEL_ARCHIVE_FOLDER,
    MODEL
)


def _new_model_name():
    return str(time.time()).replace('.', '')


def new_model_path(trained):
    return MODEL_IN_USE_FOLDER + _new_model_name() + ('.model.pt' if trained else '.tuned.pt')


def _model_files():
    all_filenames = list(os.listdir(MODEL_IN_USE_FOLDER))
    non_sys_files = []
    for filename in all_filenames:
        if filename[0] != '.' and '.' in filename:
            non_sys_files.append(filename)
    return non_sys_files


def newest_model_path():
    model_files = _model_files()
    return MODEL_IN_USE_FOLDER + sorted(model_files, reverse=True)[0]


def archive_current_models():
    'Move model files from MODEL_IN_USE_FOLDER to the MODEL_ARCHIVE_FOLDER.'
    model_files = _model_files()
    for file in model_files:
        os.rename(MODEL_IN_USE_FOLDER + file, MODEL_ARCHIVE_FOLDER + file)

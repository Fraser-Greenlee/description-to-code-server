"""
Cron job that runs to check if there's any new corrections from the user to tune on.
"""
from random import randint
from apscheduler.schedulers.blocking import BlockingScheduler
import bson

from _db import PREDICTIONS, PREDICTIONS_ARCHIVE, MAIN_PAIRS
from main import tune_to_new_data, _setup
from _make_variations import make_variations
from _make_tensor import tensors_from_pair
from _constants import DATASET_SPLIT_MAX_VALUE


def _archive_predictions(predictions):
    PREDICTIONS_ARCHIVE.insert_many(predictions)
    id_strings = [prediction['_id'] for prediction in predictions]
    PREDICTIONS.delete_many(
        {'$or': [{'_id': bson.ObjectId(id_string)} for id_string in id_strings]}
    )


def _move_into_main_pairs(predictions):
    new_pairs = []
    for prediction in predictions:
        new_pairs.append({
            'description': prediction['description']['parameterized'],
            'code': prediction['code']['confirmed']['parameterized'],
            'modules': [],
            'split_field': randint(0, DATASET_SPLIT_MAX_VALUE - 1),
            'admin_info': {
                'from_user': True,
                'checked_by_admin': False
            }
        })
    _archive_predictions(predictions)
    MAIN_PAIRS.insert_many(new_pairs)


def _update_predictions(predictions, new_attributes):
    id_strings = [prediction['_id'] for prediction in predictions]
    PREDICTIONS.update_many(
        {'$or': [{'_id': bson.ObjectId(id_string)} for id_string in id_strings]},
        {'$set': new_attributes}
    )


def _main():
    """
    Checks for predictions to tune to. If so, make variations on them and attempt to tune the model to them.
    If tuning fails, set "attempted_to_train_on" to True.
    Else, move them into `expanded_pairs` with `{from_user: True, checked_by_admin: False}`
    """
    failed_predictions = list(PREDICTIONS.find(
        {"$and": [
            {"$or": [
                {'code.generated': None},
                {'code.generated.parameterized': {'$ne': 'code.confirmed.parameterized'}}
            ]},
            {'attempted_to_train_on': {'$ne': True}},
            {'got_response': True}
        ]}
    ))
    if failed_predictions:
        try:
            failed_pairs = [
                [
                    prediction['description']['parameterized'],
                    prediction['code']['confirmed']['parameterized']
                ] for prediction in failed_predictions
            ]
            prediction_variations = make_variations(failed_pairs)
            prediction_tensors = [tensors_from_pair(DEVICE, DESCRIPTION_LANGUAGE, CODE_LANGUAGE, pair) for pair in prediction_variations]
            tune_to_new_data(prediction_tensors)
        except RuntimeError as _:
            print('failed on', len(failed_predictions), 'predictions')
            _update_predictions(failed_predictions, {'attempted_to_train_on': True})
            return False
        _move_into_main_pairs(failed_predictions)
        print('tuned to', len(failed_predictions), 'predictions')
    return True


def start():
    '''
    Runs `_main()` every minute (if `_main` takes over a minute it doesn't overlap).
    '''
    global DEVICE
    global DESCRIPTION_LANGUAGE
    global CODE_LANGUAGE
    DEVICE, DESCRIPTION_LANGUAGE, CODE_LANGUAGE, _, _, _ = _setup()
    scheduler = BlockingScheduler()
    scheduler.add_job(_main, 'interval', seconds=1)
    scheduler.start()



if __name__ == "__main__":
    DEVICE = None
    DESCRIPTION_LANGUAGE = None
    CODE_LANGUAGE = None
    start()

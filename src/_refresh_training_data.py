import datetime
import json
import bson
from tqdm import tqdm

from _db import PREDICTIONS, MAIN_PAIRS
from _make_variations import make_variations
from _make_tensor import indeces_from_pair_dict
from _load_pickle import load_pickle
from _constants import (
    DATASET_SPLIT_MAX_VALUE,
    LANGUAGE_INSTANCES_PATH,
    DESCRIPTION_INDEX,
    CODE_INDEX
)

def _inc_split_field(split_field):
    split_field += 1
    if split_field > DATASET_SPLIT_MAX_VALUE:
        split_field = 0
    return split_field


def _new_expanded_pairs_from_predictions(predictions):
    expanded_pairs = []
    split_field = MAIN_PAIRS.find({}).sort('_id', -1).limit(1)[0]['split_field']
    for prediction in predictions:
        pairs = make_variations([
            prediction['description']['parameterized'],
            prediction['code']['confirmed']['parameterized']
        ])
        for pair in pairs:
            split_field = _inc_split_field(split_field)
            expanded_pairs.append({
                "description" : pair[DESCRIPTION_INDEX],
                "code" : pair[CODE_INDEX],
                "modules" : prediction['context']['modules'],
                "split_field" : split_field,
                'added_by_user': True,
                'checked_by_admin': False,
                'added_at': datetime.datetime.now()
            })
    return expanded_pairs


def _remove_addded_predictions(predictions):
    id_strings = [prediction['_id'] for prediction in predictions]
    PREDICTIONS.delete_many(
        {'$or': [{'_id': bson.ObjectId(id_string)} for id_string in id_strings]}
    )


def _add_predictions_to_expanded_dataset():
    confirmed_predictions = list(PREDICTIONS.find({
        'code.confirmed.parameterized': {'$ne': None}
    }))
    if confirmed_predictions:
        new_expanded_pairs = _new_expanded_pairs_from_predictions(confirmed_predictions)
        MAIN_PAIRS.insert_many(new_expanded_pairs)
        _remove_addded_predictions(confirmed_predictions)


def _refresh_local_dataset(description_lang, code_lang):
    pbar = tqdm(total=DATASET_SPLIT_MAX_VALUE)
    for split_index in range(DATASET_SPLIT_MAX_VALUE):
        split_pairs = MAIN_PAIRS.find({'split_field': split_index})
        indeces = [indeces_from_pair_dict(description_lang, code_lang, pair) for pair in split_pairs]
        json.dump(indeces, open('processed_data/indeces.' + str(split_index) + '.json', 'w'))
        pbar.update(1)
    pbar.close()


def refresh_training_data(description_lang, code_lang):
    'Add predictions to the expanded dataset and save the expanded pairs to local storage.'
    # _add_predictions_to_expanded_dataset()
    _refresh_local_dataset(description_lang, code_lang)

from setuptools import setup, find_packages

setup(
    name="Description2Code Model",
    version="0.1",
    packages=find_packages(),
    scripts=['src/main.py'],
    install_requires=[
        'torch==1.0.0', 
        'matplotlib==2.0.2',
        'pandas==0.20.3',
        'flask==1.0.2',
        'StringGenerator==0.3.1',
        'pymongo==3.6.1',
        'APScheduler==3.5.3',
        'tqdm==4.28.1',
        'simplejson==3.16.0',
        'yapf==0.26.0',
        'nltk==3.2.5'
    ],
    entry_points={
        'console_scripts': [
            'train = main:train',
            'evaluate = main:evaluate',
            'interactive = main:run_interactive',
            'split_data = main:split_the_all_file_data'
        ]
    }
)

# Description To Code Server

Code for the desc2code server and model.

## Installing

Runs on Python3.

Use `setup.py` to install the dependencies: `python setup.py install`
Then try interactive mode: `python src/main.py -m interactive`

## Running the Server

1. Run the MongoDB database.
2. Run the Extension.
3. Run `src/server.py` for Deep Learning and `src/search.server.py` for Search.

## Running the Model

### Training

1. Run the dataset generator.
2. Run `src/assign_split_fields_to_dataset.py`
3. Run `src/add_tokens_from_collection.py`
4. Run `src/update_languages_using_tokens_collection.py`
5. Run `src/main.py -m train`

### Evaluating

1. Run `src/main.py -m evaluate`

### Interactive Mode

1. Run `src/main.py -m interactive`

## Project Structure

### models/

Saved models from last training session and older ones.
Sadly the language indices for old models have been lost so they aren't usable.

### processed_data/

Holds indices files each representing 1/100th of the description-code dataset.

### src/

Code for the servers and model.

### starting_state/

Saved language instances and synonym dictionary.

### tests/

Tests for the parameter handler.

### user_study_keylogs/

Unused data from the user study.

### root

Has Python notebooks used for analysis, setup file and README.

## Database

Uses MongoDB.

Backup folder: XXX
